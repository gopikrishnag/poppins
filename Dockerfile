FROM node:8


WORKDIR /app
COPY package.json  /app/
RUN npm install

COPY . /app

COPY ombiance /ombiance
RUN chmod a+x /ombiance

ENTRYPOINT ["/ombiance"]
CMD ["npm", "start"]

HEALTHCHECK --interval=10s --timeout=5s CMD  wget -O - http://localhost:8000/ || exit 1
