var express = require('express');
var router = express.Router();
var onRequest = require('../src/test');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Mary Project',
    numConnections: 22
  });
});

router.post('/test', onRequest);
module.exports = router;
