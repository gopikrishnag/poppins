var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var routes = require('./routes');
var http = require('http');
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', routes);


/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen('8000', function () {
  console.log('Server listening at port %d', '8000');
});


module.exports = app;
