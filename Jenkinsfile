timestamps {

  node {

    // == Service-specific variables ==
    //
    // Things that need to be customized for a service should be here.

    env.SERVICE = "poppins"
    env.EMAIL_TO = "nbommu@care.com kjain@care.com"

    // If you need to modify anything below this point, other than the
    // "Build the app" and "Test the build" stages (which can be
    // customized with appropriate things for this service), please let
    // prodops know.

    try {

      stage ('Set up the environment') {
        env.BUILD_NUM = VersionNumber projectStartDate: '', versionNumberString: '${BUILD_DATE_FORMATTED, "yyMMddHHmmss"}', versionPrefix: ''
        currentBuild.displayName = "${BRANCH_NAME}-#${BUILD_NUM}"
        env.LOCAL_IMAGE_NAME = "${SERVICE}:${BUILD_NUM}"
      }

      stage ('Set up the workspace') {
        checkout scm
        env.SEMVER = readFile("${WORKSPACE}/VERSION").trim()

        // The git module env vars do not show up in multi-branch pipelines
        // so we do it via git commands instead.
        // See https://issues.jenkins-ci.org/browse/JENKINS-35230
        env.GIT_BRANCH = sh(returnStdout: true, script: 'git rev-parse --abbrev-ref HEAD').trim()
        env.GIT_COMMIT = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()

        sh "/usr/local/bin/setup-workspace $workspace"
      }

      stage ('Build the app') {
        // Replace this with any steps needed to build this app!
        println "No steps needed to build this app."
      }

      stage ('Test the build') {
        // Replace this with any steps needed to test the build!
        println "No steps needed to test this build."
      }

      stage ('Build the image') {
        sh "/usr/local/bin/paramabob ${LOCAL_IMAGE_NAME}"
      }

//      stage('Scan the local image via aquasec') {
//        aqua locationType: 'local', localImage: "${LOCAL_IMAGE_NAME}", hideBase: false, notCompliesCmd: '', onDisallowed: 'ignore', showNegligible: false
//      }

      stage ('Push the image') {
        env.RELEASE_VERSIONS_FROM_MASTER_ONLY = "true"
        sh "/usr/local/bin/semver-fi ${LOCAL_IMAGE_NAME}"
      }
 
      stage('Have Aquasec scan the pushed image') {
        aqua locationType: 'hosted', registry: 'dockreg.use.dom', hostedImage: "${LOCAL_IMAGE_NAME}",  notCompliesCmd: 'exit 1', onDisallowed: 'ignore', hideBase: false, showNegligible: false
      }

      stage ('Autodeploy the stacks specified by this service') {
        sh "/usr/local/bin/autodeploy"
      }

    } //try

    finally {
      deleteDir()
      step([$class: 'Mailer',
            notifyEveryUnstableBuild: true,
            recipients: "${EMAIL_TO}",
            sendToIndividuals: true])
    }

  } //node

} //timestamps
